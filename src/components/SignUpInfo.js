const SignUpInfo = ({ formData, setFormData }) => {
	const setEmail = (e) => {
		setFormData({ ...formData, email: e.target.value })
	}
	const setPassword = (e) => {
		setFormData({ ...formData, password: e.target.value })
	}
	const setConfirmPassword = (e) => {
		setFormData({ ...formData, confirmPassword: e.target.value })
	}

	return (
		<div className='sign-up-container'>
			<input
				type='text'
				placeholder='Email...'
				value={formData.email}
				onChange={setEmail}
			/>
			<input
				type='text'
				placeholder='Password'
				value={formData.password}
				onChange={setPassword}
			/>
			<input
				type='text'
				placeholder='Confirm password...'
				value={formData.confirmPassword}
				onChange={setConfirmPassword}
			/>
		</div>
	)
}
export default SignUpInfo
