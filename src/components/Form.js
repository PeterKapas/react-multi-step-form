import { useState } from 'react'
import OtherInfo from './OtherInfo'
import SignUpInfo from './SignUpInfo'
import PersonalInfo from './PersonalInfo'

const Form = () => {
	const [page, setPage] = useState(0)
	const [formData, setFormData] = useState({
		email: '',
		password: '',
		confirmpassword: '',
		firstname: '',
		lastname: '',
		age: '',
		username: '',
		nationality: '',
		other: '',
	})
	const formTitles = ['Sign Up', 'Personal Info', 'Other']
	const pageDisplay = () => {
		if (page === 0)
			return <SignUpInfo formData={formData} setFormData={setFormData} />
		if (page === 1)
			return <PersonalInfo formData={formData} setFormData={setFormData} />
		if (page === 2)
			return <OtherInfo formData={formData} setFormData={setFormData} />
	}

	return (
		<div className='form'>
			<div className='progressbar'>
				<div
					style={{
						width: page === 0 ? '33.3%' : page === 1 ? '66.6%' : '100%',
					}}
				></div>
			</div>
			<div className='form-container'>
				<div className='header'>
					<h1>{formTitles[page]}</h1>
				</div>
				<div className='body'>{pageDisplay()}</div>
				<div className='footer'>
					<button
						disabled={page === 0}
						onClick={() => {
							setPage((currPage) => currPage - 1)
						}}
					>
						Prev
					</button>
					<button
						onClick={() => {
							if (page === formTitles.length - 1) {
								alert('Form submitted')
								console.log(formData)
							} else {
								setPage((currPage) => currPage + 1)
							}
						}}
					>
						{`${page === 0 || page === 1 ? 'Next' : 'Submit'}`}
					</button>
				</div>
			</div>
		</div>
	)
}
export default Form
