const OtherInfo = ({ formData, setFormData }) => {
	const setNationality = (e) => {
		setFormData({ ...formData, nationality: e.target.value })
	}
	const setOther = (e) => {
		setFormData({ ...formData, other: e.target.value })
	}
	return (
		<div className='other-info-container'>
			<input
				type='text'
				placeholder='Nationality'
				value={formData.nationality}
				onChange={setNationality}
			/>
			<input
				type='text'
				placeholder='Other...'
				value={formData.other}
				onChange={setOther}
			/>
		</div>
	)
}
export default OtherInfo
