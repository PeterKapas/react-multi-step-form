const PersonalInfo = ({ formData, setFormData }) => {
	const setFirstName = (e) => {
		setFormData({ ...formData, firstname: e.target.value })
	}
	const setLastName = (e) => {
		setFormData({ ...formData, lastname: e.target.value })
	}
	const setUserName = (e) => {
		setFormData({ ...formData, username: e.target.value })
	}
	const setAge = (e) => {
		setFormData({ ...formData, age: e.target.value })
	}

	return (
		<div className='personal-info-container'>
			<input
				type='text'
				placeholder='First Name'
				value={formData.firstname}
				onChange={setFirstName}
			/>
			<input
				type='text'
				placeholder='Last Name'
				value={formData.lastname}
				onChange={setLastName}
			/>
			<input
				type='text'
				placeholder='Username'
				value={formData.username}
				onChange={setUserName}
			/>
			<input
				type='text'
				placeholder='Age'
				value={formData.age}
				onChange={setAge}
			/>
		</div>
	)
}
export default PersonalInfo
